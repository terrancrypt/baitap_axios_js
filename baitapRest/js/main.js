function tinhDiemTrungBinh (...Diem){

    let tongDiem = 0;
    let diemTrungBinh = 0;
    for ( let i = 0; i < Diem.length; i++){
        tongDiem += Diem[i];
    }
    diemTrungBinh = tongDiem / Diem.length;
    return diemTrungBinh;

}

let tinhDiemKhoi1 = (diemToan, diemLy, diemHoa) => {
    diemToan = document.getElementById('inpToan').value * 1;
    diemLy = document.getElementById('inpLy').value * 1;
    diemHoa = document.getElementById('inpHoa').value * 1;
    
    let dTB = tinhDiemTrungBinh(diemToan, diemLy, diemHoa);

    document.getElementById('tbKhoi1').innerText = dTB.toPrecision(3);
}

let tinhDiemKhoi2 = (diemVan, diemSu, diemDia, diemTiengAnh) => {
    diemVan = document.getElementById('inpVan').value * 1;
    diemSu = document.getElementById('inpSu').value * 1;
    diemDia = document.getElementById('inpDia').value * 1;
    diemTiengAnh = document.getElementById('inpEnglish').value * 1;
    
    let dTB = tinhDiemTrungBinh(diemVan, diemSu, diemDia, diemTiengAnh);

    document.getElementById('tbKhoi2').innerText = dTB.toPrecision(3);
}