const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

console.log(colorList);

var colorListCopy = colorList.slice();
console.log(colorListCopy);

let loadOMau = () => {
  let addHtml = "";
  for (let i = 0; i < colorList.length; i++) {
    addHtml += `
        <button id="button${i}" class="color-button ${colorList[i]}"></button>`;
  }
  document.getElementById("colorContainer").innerHTML = addHtml;
};
loadOMau();

// Hàm mặc định phần tử trong giao diện
let inActive = () => {
  // Gọi tất cả các phần tử trong thẻ colorContainer ra
  let btnContainer = document.getElementById("colorContainer");
  // Gọi tất cả các button có class color-button trong colorContainer ra
  let btns = btnContainer.getElementsByClassName("color-button");
  console.log("button", btns);
  let house = document.getElementsByClassName('house')
  // Thêm class active vào phần tử đầu tiển trong mảng btns
  btns[0].classList.add("active");
  // Dùng vòng lặp để thêm class active vào button hiện tại được nhấp
  for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function () {
      let current = document.getElementsByClassName("active");
      // Xóa class active đi nếu có nhiều hơn 0 phần tử có class active
      if (current.length > 0) {
        current[0].className = current[0].className.replace(" active", "");
        document.getElementById("house").className = "house";
      }
      // Thêm class active vào phần tử được click hiện tại
      this.className += " active";
      console.log(this.className);

      house[0].classList.add(colorList[i]);
      let colorCurrent = house[0].classList[1];
      console.log(colorCurrent);
    });
  }
};
inActive();